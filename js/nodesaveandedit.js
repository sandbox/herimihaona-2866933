/**
 * @file
 * Defines Javascript behaviors for the nodesaveandedit module.
 */

(function ($, Drupal, drupalSettings) {
	'use strict';
	$('<a href="' + drupalSettings.a.href + '">' + drupalSettings.a.text + '</a>').insertBefore('#edit-actions');
	$('#edit-actions li.unpublish input').val($('#edit-actions li.unpublish input').val() + drupalSettings.a.ext);
	$('#edit-actions li.publish input').val($('#edit-actions li.publish input').val() + drupalSettings.a.ext);
})(jQuery, Drupal, drupalSettings);